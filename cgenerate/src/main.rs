use std::{
    io::Write,
    path::{Path, PathBuf},
};

use clap::Parser;
use fnv::FnvHashMap;
use itertools::Itertools;
use lsharp_ru::definitions::{
    mealy::{InputSymbol, Mealy, OutputSymbol, State},
    FiniteStateMachine,
};
use serde::{Deserialize, Serialize};

mod cli;

#[derive(Serialize, Deserialize)]
struct CgenInfo {
    inputs_vec: Vec<(InputSymbol, String)>,
    outputs_vec: Vec<(OutputSymbol, String)>,
    traces_vec: Vec<Vec<InputSymbol>>,
}

// TODO: Change all CLI arguments to instead point to a file.

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = cli::Cli::parse();

    let info_file_path = args.info();
    let info_file_json =
        std::fs::read_to_string(info_file_path).expect("Could not read info file!");
    let CgenInfo {
        inputs_vec,
        outputs_vec,
        traces_vec,
    } = serde_json::from_str(&info_file_json).expect("Could not deserialize json.");

    let enable_crash = *args.crash();
    let input_map: FnvHashMap<_, _> = inputs_vec.into_iter().collect();
    let output_map: FnvHashMap<_, _> = outputs_vec.into_iter().collect();

    let rev_input_map: FnvHashMap<_, _> = input_map.iter().map(|(k, v)| (v.clone(), *k)).collect();

    let rev_output_map: FnvHashMap<_, _> =
        output_map.iter().map(|(k, v)| (v.clone(), *k)).collect();

    let mut out_code = String::new();

    out_code.push_str(HEADER);
    let input_file = PathBuf::from(args.input());
    let fsm = lsharp_ru::util::parsers::machine::read_mealy_from_file_using_maps(
        input_file,
        &rev_input_map,
        &rev_output_map,
    )?;
    let reset_symbol = InputSymbol::new(rev_input_map.len() as u16);
    let out_dir = PathBuf::from(args.output());
    println!("Creating out directory...");
    let _ = std::fs::remove_dir_all(&out_dir);
    let _ = std::fs::create_dir(&out_dir);

    println!("Creating dictionary...");
    let mut dict_name = out_dir.to_str().expect("Safe").to_string();
    dict_name.push_str(".dict");
    {
        let dict_file_path = out_dir.join(dict_name);
        let mut dict_file = std::fs::File::create(dict_file_path)?;
        for i in fsm.input_alphabet() {
            let i_str = i.raw().to_string();
            writeln!(&mut dict_file, "\"{}\"", i_str).expect("Error while writing dict!");
        }
    }

    println!("Creating out file...");
    let mut file_name = out_dir.to_str().expect("Safe").to_string();
    file_name.push_str(".c");
    let out_file = out_dir.clone().join(file_name);
    println!("{:?}", out_file);
    let mut out_file = std::fs::File::create(out_file)?;

    println!("Reading traces...");
    let traces = traces_vec;
    println!("Modifying traces to crash...");
    // let tests_for_afl_crashing = crash_tests(tests_for_afl, fsm.input_alphabet());
    println!("Creating test files...");
    write_tests(&traces, &out_dir);

    let sink_output = if enable_crash {
        *rev_output_map
            .get(&"error".to_string())
            .ok_or("This script is only meant to be used with the ASML partial models.")?
    } else {
        OutputSymbol::new(u16::MAX)
    };
    out_code.extend(
        fsm.states()
            .iter()
            .copied()
            .map(|s| gen_state_fn(s, &fsm, sink_output, reset_symbol)),
    );

    let step_str = gen_step_fn(&fsm);
    out_code.push_str(&step_str);

    out_code.push_str(DEFAULT_STATE_CASE);

    out_code.push_str(FOOTER);

    out_file.write_all(out_code.as_bytes())?;
    Ok(())
}

fn crash_tests(
    tests_for_afl: Vec<Vec<InputSymbol>>,
    inputs: Vec<InputSymbol>,
) -> Vec<Vec<InputSymbol>> {
    let append_symbols = |trace: Vec<InputSymbol>| {
        inputs
            .iter()
            .take(5)
            .map(|i| {
                let mut ret = trace.clone();
                ret.push(*i);
                ret
            })
            .collect::<Vec<Vec<InputSymbol>>>()
    };
    tests_for_afl.into_iter().flat_map(append_symbols).collect()
}

fn write_tests(tests_for_afl: &[Vec<InputSymbol>], out_dir: &Path) {
    let test_dir = out_dir.join("test_dir");
    std::fs::create_dir(&test_dir).expect("Could not create test_dir");
    for (num_test, trace) in tests_for_afl.iter().enumerate() {
        let file_path = test_dir.join(format!("{}.test", num_test));
        let mut file = std::fs::File::create(file_path).expect("Could not create test file");
        let trace: String = trace
            .iter()
            .map(|i| {
                let mut x = i.raw().to_string();
                x.push(' ');
                x
            })
            .collect();
        file.write_all(trace.as_bytes()).expect("Safe");
    }
}

fn extract_input_traces(logs: &[(Vec<InputSymbol>, Vec<OutputSymbol>)]) -> Vec<Vec<InputSymbol>> {
    logs.iter().map(|x| x.0.clone()).collect()
}

fn gen_step_fn(fsm: &Mealy) -> String {
    let mut ret = String::new();
    let decl = "int step(int input) {\n\tswitch (current_state) {";
    ret.push_str(decl);
    for s in fsm.states() {
        let step_str = format!(
            "\t\tcase {}:\n\t\t\t return step_state_{}(input);\n",
            s.raw(),
            s.raw()
        );
        ret.push_str(&step_str);
    }
    ret
}

const HEADER: &str = "#include <stdio.h>
#include <stdlib.h>

// Exit directly if we have an invalid input.
void invalid_input(){
    exit(-1);
}

int current_state = 0;
";

const DEFAULT_STATE_CASE: &str = "
            default:
                    invalid_input();
            }
            return -1;
}
";

const FOOTER: &str = r#"
int main() {
            #ifdef __AFL_HAVE_MANUAL_CONTROL
                    __AFL_INIT();
            #endif
            
            // main i/o loop
            while (__AFL_LOOP(1000)) {
            // while (1) {
                    // read input
                    int input = 0;
                    int ret = scanf("%d", &input);
                    if (ret == EOF){
                        exit(0);
                    } else if (ret == 0) {
                        invalid_input();
                    } else {
                        // operate state machine
                        int i = step(input);
                        printf("%d\n", i);
                            fflush(stdout);
                    }
                }
}"#;

fn gen_state_fn(
    state: State,
    fsm: &Mealy,
    sink_out: OutputSymbol,
    reset_input: InputSymbol,
) -> String {
    let mut ret = String::new();
    let fn_decl = format!(
        "int step_state_{}(int input) {{\n\t switch(input) {{",
        state.raw()
    );
    let initial_st = fsm.initial_state();
    ret.push_str(fn_decl.as_str());
    for input in &fsm.input_alphabet() {
        let (dest, out) = fsm.step_from(state, *input);
        let tr_decl = if out == sink_out {
            format!("\n\t\tcase {}:\n\t\t\t invalid_input();", input.raw())
        } else {
            format!(
                "\n\t\tcase {}:\n\t\t\t current_state = {};\n\t\t\treturn {};",
                input.raw(),
                dest.raw(),
                out.raw()
            )
        };
        ret.push_str(&tr_decl);
    }
    let reset_transition = format!(
        "\n\t\tcase {}:\n\t\t\t current_state = {};\n\t\t\t;",
        reset_input.raw(),
        initial_st.raw()
    );
    ret.push_str(&reset_transition);
    ret.push_str(DEFAULT_STATE_CASE);

    ret
}
