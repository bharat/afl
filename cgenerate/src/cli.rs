use clap::Parser;

#[derive(Parser, derive_getters::Getters)]
#[command(author, version, about)]
pub struct Cli {
    #[arg(short = 'i')]
    /// Path to the DOT file.
    input: String,
    #[arg(short = 'o')]
    /// Path for the generated C file.
    output: String,
    #[arg(long)]
    /// Path for the info file.
    info: String,
    #[arg(long)]
    /// Crash if set on error output.
    crash: bool,
}
